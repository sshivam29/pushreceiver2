package com.jio.mhood.poc.push.receiver2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class PushBroadcastReceiver2 extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		
		if(intent.getAction() == "com.jio.mhood.push.intent.RECEIVE"){
			
			Toast.makeText(context, "Push Receiver 2 called", Toast.LENGTH_SHORT).show();
			String pushMesg = intent.getStringExtra("pushMesg");
			if(pushMesg!=null){
				Toast.makeText(context, "Pushed Message: " + pushMesg, Toast.LENGTH_SHORT).show();	
			}else{
				Toast.makeText(context, "No Pushed Message Received", Toast.LENGTH_SHORT).show();
			}
		}

	}

}
